---
title: Modularity Lab - Deploying with Apache ACE
author: Luminis
layout: base
---

Part 4 - Deployment
===========================================
In this part of the lab we will deploy the application we created earlier.

There are several ways to deploy an OSGi application. We will try two of them: 

* Runnable JAR files
* Apache ACE

Runnable JAR files
--------------------------------
Before we can deploy an application, we should first be able to build it outside of Eclipse. Bndtools offers Gradle support, which is available without doing any work yourself.

Open a shell and navigate to the workspace directory. Now execute <code>./gradlew build</code>. This will start a full build, which also runs unit and integration tests. The build should complete without errors. 

If the build was successful, let's try to build an executable JAR. An executable JAR contains the OSGi framework and all the application's bundles, and can be started by <code>java -jar myapp.jar</code>. We can build the JAR by starting <code>./gradlew export</code>. The JAR file can be found in "[projectname]/generated/distributions". Make sure to copy the "conf" directory to the same directory as the JAR file before starting it.


Apache ACE
--------------------------------

Because of the flexibility that modular applications give you, the number of different ways to combine and deploy those modules increases. Especially if you deploy them to lots of targets, you end up pretty quickly with a situation you cannot handle manually any more. This is where Apache ACE comes in. It allows you to define exactly what modules should be combined and deployed to what targets.

Apache ACE consists of a server and targets that can be any OSGi container, as long as they have a "management agent" bundle installed.

The server consists of a repository, like the repositories that Bndtools uses, and a data store that keeps track of all metadata and relations between the artifacts in a repository and the targets that are being managed. Because there can be many artifacts and many targets, the server uses two levels of grouping in between to make them more manageable:

* Features group a number of usually related artifacts into a single unit (hence the name feature). Think of them like the plugable features or building blocks of your application.
* Distributions group a number of features. Distributions are typically the different configurations of features that make up a working application.

Finally, distributions can be associated with targets. All artifacts that are associated through features and distributions will end up on a target. Even though an artifact can be associated with multiple features or distributions, you will end up with only one copy of such an artifact on the target.

The management agent on a target will poll the server periodically to check for updates.
Every time the server makes a change to the artifacts for a specific target, it will create a new version of the software for that target. All targets will start at version 1, and that version will be bumped only when necessary. That means that version 1 for target A can be different from version 1 for target B and at some point in the future, target A might be at version 5, while B might be at 3.

## Getting started

We will start by installing Apache ACE. We've included a binary release of the server that you can unpack and run. Make sure you stop the running application first to avoid a port conflict!

	unzip apache-ace-2.0.1-bin.zip
	cd apache-ace-2.0.1-bin
	java -jar server-allinone.jar

Once the server has been unpacked and started, you can point your browser at <http://localhost:8080/ace/> and a login prompt should appear. Log in with username 'd' and password 'f' and you end up with a user interface that shows 4 columns side-by-side with nothing in them.

## Step 1: Releasing our bundles

We will once again use Gradle to create a directory containing all bundles in our run configuration. Move into the agenda project and execute: <code>./gradle runbundles</code>. The bundles will be available in "generated/distributions/runbundles".

## Step 2: Adding all the bundles

Before we can start distributing software, we first need to upload it to the repository. Out of the box, the ACE server ships with a repository that is stored on the local server. In the Apache ACE user interface, click the "Add artifact..." button. A dialog will open. In this dialog you can either use drag and drop to drag multiple bundles into the bottom table of the dialog, or use the "Upload" button to select files one by one. The topmost table shows artifacts that were already in your repository, but that you did not yet import into Apache ACE. For this repository that list is usually empty, but if you were to hook up Apache ACE to a large existing repository, that is were you could select existing artifacts and start using them.

Drag all the bundles in the "runbundles" folder to Apache ACE. Click the "add" button, and after the dialog disappeared, click the "store" button.

To be able to work with our configuration file, we also need to install the AutoConf Resource Processor. Open the "Add" dialog again, select AutoConf Resource Processor in the top part of the dialog and click "add" and "save" again. You can now also add the configuration file itself. 

The ACE user interface works much like a revision control system. When you want to make changes, you first 'Retrieve' the current version from the server (done automatically when you login). Then you can start making changes as much as you like. Finally you 'Store' those changes back to the server.

## Step 3: Creating features and distributions

The next step is to define the features and distributions that make up our application. Let's create two features for our application. One feature containing just the Gogo shell, and one feature containing our app's bundles. Create the two features and drag the bundles to the feature. Now also create a distribution "demo" and (for now) just drag the Gogo feature to the distribution. When we're done, hit 'Store' again to persist our work.

> Apache ACE uses an OSGi standard called Deployment Admin to install artifacts. This standard has an extension mechanism that allows you to provision new file types, such as configuration data, using resource processors. Resource processors are bundles that know how to locally install new file types. ACE makes sure that resource processors are automatically shipped whenever you ship a file type to a target. This mechanism is very powerful as it allows you to install almost anything you can imagine, and decide to do so without having to restart the target.

## Step 4: Defining a target and deploying distributions

The next step is to bootstrap an empty target and deploy a distribution to it. 

ACE comes with both a management agent bundle that you can drop into an existing framework and a launcher that embeds both a framework and the management agent. We will use the latter here. Open a new shell window. Create a new folder somewhere, and copy the ace-launcher.jar that you can find in the server-allinone/store folder into that new folder. Now, launch the target as follows:

	java -Dorg.osgi.service.http.port=8000 -Dagent.identification.agentid=target1 -jar ace-launcher.jar

We explicitly set the HTTP port to 8000 to avoid a port conflict with the Apache ACE server. Of course we could have done this using a configuration file as well. We also explicitly set a name for the target. If you now look at the Apache ACE UI again and hit "retrieve", you should see the target in the column on the right.

Now it's time to associate a distribution with our target, so our application gets provisioned. Again, we do this using drag and drop. When done, hit 'Store' to commit the changes again. After a few seconds you should see the shell started at the target.

Let's make a change now and also associate the application's feature to the distribution. Click store again, and see the running target being updated.

> When the management agent running in a target updates the bundles, you usually don't see anything happening. Unless your framework is running a shell, there is no direct way to interact with it. However, the management agent does keep track of a log and sends this log to the server. If you doubleclick on a target and select the LogViewer tab, you can take a look at this log. It is in reverse chronological order and contains all life cycle changes: bundles starting, stopping, updating and deployments starting and stopping. If a deployment for some reason fails, it gets rolled back. If this log still does not tell you what went wrong, you can either try to install a shell and a log service, or even the Felix WebConsole, or you can restart your target with the JVM option: `-Dlog=true` which will output all information from the log service to your console.

## Step 5: Running another target

To validate that we can indeed provision multiple targets, we can run another target on our machine. There are a couple of things we need to consider:

* We are running a webserver on each target, so we need to configure each additional target to use a different, unique port.
* An OSGi framework will typically use the current directory to store it's bundle cache, so we need to make sure we start each target from a new folder (or reconfigure the framework to use a different folder).
* Each target needs a unique ID.

So create a new folder, copy the `ace-launcher.jar` to it, and launch the target as follows:

	java -Dorg.osgi.service.http.port=8001 -Dagent.identification.agentid=target2 -jar ace-launcher.jar

That should give you a second target, which you can now provision by associating a distribution to it.

## Step 6: Updating a bundle

Our next step is to update some code in a bundle. Let's enhance our console bundle a bit with some pretty printing of the list of conferences:

	public void listConferences() {
		List<Conference> conferences = agenda.listConferences();
		if (conferences != null) {
			System.out.println("Conferences:");
			for (Conference conference : conferences) {
				System.out.println(" * " + conference.getName());
			}			
		} else {
			System.out.println("Agenda is not available.");
		}
	}

Now make sure to update the version of the bundle in the bundle descriptor. Apache ACE understands semantic versioning and will only send updates to targets when a newer version is available. Upload the new bundle to Apache ACE. Once uploaded, you will notice that the new version automatically replaced the old version in the feature defined in Apache ACE. This happens because by default, features that have an association with a bundle, will automatically be associated with the highest version of that bundle. So, by simply uploading a new version of a bundle, all features containing that bundle will be updated. This change will cascade all the way up to the affected targets. Check if the change worked by invoking the shell command to list the conferences again. You can also type 'lb' to list the bundles. You should see the new version of the console bundle in that list.

Today we have just provisioned targets on our local machine. For real projects we could of course deploy the Apache ACE server and targets on cloud nodes. We would also automate deployments to Apache ACE completely by using scripting (Apache ACE is fully scriptable). Running on a cloud platform we could enable auto-scaling and automated failover that way. An Apache ACE target could also very well be some kind of device; really anything that runs OSGi can be a target!

*That's it! You have reached the end of these hands-on labs on Building Modular Java Enterprise Applications. We hope you enjoyed the labs and have lots of inspiration for creating your own modular applications. If you need more information, or want to contact us for another matter, please take a look at the Links section.*
