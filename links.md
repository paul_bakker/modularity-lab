---
title: Modularity Lab
author: Luminis
layout: base
---


## Links

* Bndtools - <http://bndtools.org/>
* Amdatu - <http://amdatu.org/>
* Apache ACE - <http://ace.apache.org/>
* Apache Felix - <http://felix.apache.org/>
* Luminis Technologies - <http://www.luminis-technologies.com>

## Contact Data

* Marcel Offermans ([@m4rr5](http://twitter.com/m4rr5)) - <marcel.offermans@luminis.eu>
* Paul Bakker ([@pbakker](http://twitter.com/pbakker))- <paul.bakker@luminis.eu>
* Marian Grigoras ([@mggrig](https://twitter.com/mggrig))- <marian.grigoras@luminis.eu>
