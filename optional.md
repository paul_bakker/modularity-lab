---
title: Modularity Lab - Extras
author: Luminis
layout: base
---

Part 4 - Additional compontents
=================================
In this part of the lab we will be looking at a few more Amdatu components that you can use to create applications. If you're short on time you can just pick the topics that you're most interested in.

Scheduling
----------------
It's common to have some scheduled tasks in an application that for example run daily on set times, or on set intervals. Amdatu Scheduling makes this as easy as annotating a service with a few extra annotations.

First make sure to add the component to both the build path and run configuration. You will need two bundles:

* org.amdatu.scheduling.api
* org.amdatu.scheduling.quartz

Now create a new service that implements the <code>org.quartz.Job</code> interface. Add some code in the execute method that you want to run every time the tasks runs. 

The final step to make this work is to configure when the task should run. You can do so using annotations. For example: 

    @RepeatForever
    @RepeatInterval(period=RepeatInterval.SECOND, value = 5)
    @Component
    public class ExampleJob implements Job { 
    	//...

Once the application is running your execute method should be executed every five seconds. Instead of working with <code>@RepeatInterval</code> you can also use full cron expressions by using the <code>@Cron</code> annotation.


Remote
----------------
OSGi has a specification to work with remote services (almost) transparantly. In this part of the lab we will distribute our agenda application, so that the agenda service runs in one process, while the rest of the application runs in another process. Distributing components over the network has of course a performance impact; network calls are significantly slower than method calls within the same machine. Although distributing components is an architectural choice, the Remote Services specification does make this possible with only minimal changes to the code. This makes it an ideal canidate to gradually move an OSGi application towards a distributed micro services architecture.

To make a service available remote we have to set a service property on that service. Let's try making the agenda.simple service available remote.

    @Component(properties=@Property(name=RemoteConstants.SERVICE_EXPORTED_INTERFACES, value="agenda.api.Agenda")) 
    public class SimpleAgendaService implements Agenda{
    	//...

That's it!

Now we have to create two new run configurations to split the components to two different processes. Let's start with the configuration for the remote agenda service.

    -runstorage: ${target}/fw-${random}
    -runbundles: \
		agenda.api;version=latest,\
		agenda.simple;version=latest,\
		org.amdatu.remote.admin.http,\
		org.amdatu.remote.topology.promiscuous,\
		org.apache.felix.dependencymanager,\
		org.apache.felix.dependencymanager.runtime,\
		org.apache.felix.dependencymanager.shell,\
		org.apache.felix.metatype,\
		org.apache.felix.eventadmin,\
		org.apache.felix.configadmin,\
		org.apache.felix.log,\
		org.apache.felix.http.jetty;version=2.3.0,\
		org.apache.felix.http.api;version=2.3.0,\
		org.apache.felix.http.servlet-api;version=1.0.0,\
		org.apache.felix.http.whiteboard;version=2.3.0,\
		org.osgi.service.remoteserviceadmin,\
		org.apache.felix.gogo.command,\
		org.apache.felix.gogo.runtime,\
		org.apache.felix.gogo.shell,\
		org.amdatu.remote.discovery.slp
	
    -runproperties: org.osgi.service.http.port=9999

Note that we're setting the bundle storage to a random directory, otherwise both run configuration would write to the same framework cache. Also, the HTTP port is explicitly set to 9999.

Next we create a configuration for the agenda client.

    -runbundles: \
		agenda.api;version=latest,\
		agenda.commands;version=latest,\
		agenda.rest;version=latest,\
		org.apache.felix.gogo.command,\
		org.apache.felix.gogo.runtime,\
		org.apache.felix.gogo.shell,\
		org.apache.felix.http.jetty,\
		org.apache.felix.http.api,\
		org.apache.felix.http.servlet-api,\
		org.apache.felix.http.whiteboard,\
		org.amdatu.remote.topology.promiscuous,\
		org.amdatu.remote.discovery.slp,\
		org.apache.felix.dependencymanager,\
		org.apache.felix.dependencymanager.runtime,\
		org.apache.felix.dependencymanager.shell,\
		org.apache.felix.metatype,\
		org.apache.felix.eventadmin,\
		org.apache.felix.configadmin,\
		org.apache.felix.log,\
		org.osgi.service.remoteserviceadmin,\
		com.fasterxml.jackson.core.jackson-annotations,\
		com.fasterxml.jackson.core.jackson-core,\
		com.fasterxml.jackson.core.jackson-databind,\
		org.amdatu.remote.admin.http;version=0.1.2
	-runproperties: org.osgi.service.http.port=8888

Try to start both. After a few seconds (discovery takes a while) the application should work as it previoiusly did. To make sure we're not being cheated, try turning of the remote agenda and see what happens to the client.

Both the discovery and trasport implementation can be swapped out. In this setup we have been using SLP for discovery, but there are other mechanism available as well, working with a broad range of network configurations.
