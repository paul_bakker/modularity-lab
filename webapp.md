---
title: Modularity Lab - Building webapps
author: Luminis
layout: base
---

Part 3 - Creating a RESTful web application
==========================================
This exercise builds upon the previously created project where we have explored the basics of a modular approach towards application development. Then we used MongoDB to create a persistent service that uses a NoSQL backend store. In this lab we will be enhancing our application by adding some RESTful services on top of it, making it even easier to use in a cloud. RESTful services are a typical way of interacting with the API of most cloud applications. 


Getting started
---------------
Let's take a look at what we need in order to get a web application up and running. We will be using JAX-RS to create our RESTful web service, so we will need some new build time dependencies. Use Amdatu Bootstrap to install the dependencies to the build path using the <code>rest-install</code> command. We can implement the first resource.


Step 1: Implement a hello world style Resource
----------------------------------------------
In order to start building the hello world service we first need to create a new Bundle Descriptor. Let's call it <code>rest</code>. First we will create a helloworld resource to test our setup.

Let's create a new Java class called <code>HelloWorldResource</code> in the package <code>agenda.rest</code>. This class represents our RESTful resource. Add a new method that returns a <code>String</code> ("hello world" of course). Now annotate the class with the JAX-RS <code>@Path("helloworld")</code> annotation. Annotate the method you just added with <code>@GET</code> and <code>@Produces(MediaType.TEXT_PLAIN)</code>. Once this class is registered as an OSGi service, it will automatically be registered as a RESTful service. The Amdatu REST component takes care of the magic under the covers. Complete the code by annotating the class <code>@Component(provides=Object.class)</code>. We have to explicitly set the provides type to Object.class for the service to be picked up by the Amdatu REST component. 

Now go to the Contents tab of the <code>rest.bnd</code> configuration file, and drag the <code>agenda.rest</code> package into the Private Packages part. Finally, we have to add new dependencies to our run configuration. We need the newly created bundle, a http server, Amdatu REST and it's dependencies. Use Amdatu Bootstrap's <code>rest-run</code> command to add the dependencies, and take a look at the .bndrun file to see the changes.  

Now, open a web browser, navigate to [http://localhost:8080/helloworld](http://localhost:8080/helloworld) and admire your first modular RESTful service!


Step 2: Implementing the Agenda Resource
----------------------------------------
Ok, that was fun, but let's start creating a real RESTful resource now. In this case we are creating a resource on top of our <code>Agenda</code> implementation. Create a new class called <code>AgendaResource</code>, annotate it with <code>@Path("agenda")</code> and <code>@Component(provides=Object.class)</code>, and add a method to list the conferences. In this method we will be using Jackson to map Java objects to JSON and vice versa. Jackson (de)serialisation is supported by Amdatu REST out of the box.

In order for our method to work we need a reference to the <code>Agenda</code> service. Add a <code>volatile</code> field to hold the reference to the service and annotate it with <code>@ServiceDependency</code>. Now use this field to retrieve the list of conferences. Since the output of our method will be JSON, we have to set the right <code>MediaType</code> using the <code>@Produces</code> annotation. We can just return the list of conferences from the method, Jackson will take care of the mapping to JSON automatically.

Your code will probably look somewhat like this:

    @GET
	@Path("conferences")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Conference> listConferences() { 
		return agendaService.listConferences();
	}
	
The end result of this step can be seen in the web browser. Navigate to [http://localhost:8080/agenda/conferences](http://localhost:8080/agenda/conferences) and check if the conferences will be listed as JSON.

*Remember, if you want to distinguish between service implementations of the <code>Agenda</code> you can add the <code>persistent=true</code> service property filter to the <code>Activator</code> of the <code>AgendaResource</code>.*

Step 3: Extending the Agenda Resource
-------------------------------------
Next, we are going to add a method to the <code>AgendaResource</code> to add a new conference to the list of conferences held within the <code>Agenda</code> service. Add a method that takes a <code>Conference</code> argument as input and annotate it with the correct JAX-RS annotations.

	@POST
	@Path("conferences")
	@Consumes(MediaType.APPLICATION_JSON)
	public void addConference(Conference conference) {
		agendaService.addConference(conference);
	}

The easiest way to test posting to a REST resource is to use the command line tool <code>curl</code> on Mac or Linux. If you are using Windows you can download [cUrl for Windows](http://curl.haxx.se) or use another REST client tool or browser-based plugin. 

    curl -i -H "Content-type: application/json" -X POST -d "{\"name\":\"JavaOne\",\"location\":\"San Francisco\"}" http://localhost:8080/agenda/conferences

If the POST command executes successfully, you can check whether the list of conferences is updated by running the GET request from the browser once again: [http://localhost:8080/agenda/conferences](http://localhost:8080/agenda/conferences) 

Step 4: Monitoring REST requests
-------------------------------------
There is an Amdatu component that gives some insights in how your REST backend is used by just dropping in some more bundles. Add the following bundles to your run configuration:

* org.amdatu.web.requestlog.api
* org.amdatu.web.requestlog.filter
* org.amdatu.web.requestlog.mongo
* org.amdatu.web.requestlog.rest
* org.amdatu.web.requestlog.webviewer
* org.amdatu.web.resourcehandler

Now fire a few requests to your Agenda resource. After doing so, open a browser and navigate to [http://localhost:8080/requestlogviewer/index.html](http://localhost:8080/requestlogviewer/index.html). The component stores request logs in Mongo, but the mechanism is completely plugable, so you can plugin other storage backends as well. 

*You have reached the end of part 3 of this hands-on lab. By now you should have a pretty good idea of what it takes to get RESTful services up and running while exposing them as OSGi Services and thus preserving modularity at both the design and runtime level. In the next part of this hands-on lab we are going to look at deployments and use Apache Ace to demonstrate a modular deployment strategy for your application.*