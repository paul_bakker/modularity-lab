---
title: Modularity Lab
author: Luminis
layout: base
---

Part 1 - Building Modular Java Applications
==========================================
The goal of this hands-on lab is to get some experience in building modular applications using OSGi and enterprise technologies that may or may not run in the cloud. You will see the following technology in action:

* OSGi bundles
* OSGi services
* Service dynamics
* NoSQL storage with MongoDB
* Restful services with JAX-RS
* Modular deployment with Apache ACE

The application you will be developing as part of this lab is a simple conference agenda.

Prerequisites 
-------------
The following items are required for this hands-on lab:

* Operating system: Win32 / Win64 / Linux64 / OS X (recent)
* A Java Development Kit (JDK 6 or higher)
* Eclipse, with the Bndtools plugin installed.

We assume you have a current JDK installed on your system. On the USB stick that was provided to you at the beginning of the lab, are different folders with installation files depending on your specific Operating System. Amongst other things, it contains an archive containing Eclipse with the Bndtools plugin pre-installed. Unarchive it to a location of your liking and fire up Eclipse to start this lab.

Getting started
---------------
After installing and starting Eclipse, let's start the lab by creating a new Bndtools OSGi Project using the Eclipse Bndtools plugin. The project name should be <code>agenda</code>. Click Next and choose Empty Project on the next screen and click Finish.  
Bndtools will now ask if you want to create a <code>cnf</code> project. The <code>cnf</code> project contains configuration for repositories and all your other projects in the workspace. A repository contains the bundles that you depend on, such as frameworks and libraries. You can compare a bundle repository to a Maven repository; it contains JAR files with metadata. The metadata for a Maven JAR file is in the <code>pom.xml</code>, and the metadata for an OSGi bundle is in the bundle's <code>manifest</code> file. Use the default <code>Bundle-Hub Configuration</code> template for the cnf project. We also recommend switching to the Bndtools perspective, as it brings a few useful views for OSGi development.

The <code>agenda</code> project is almost a plain Java project, besides the <code>bnd.bnd</code> file. The <code>bnd.bnd</code> file contains all the configuration to build a bundle. Let's start writing some code!

Step 1: Creating an API bundle
------------------------------
In Bndtools you have a choice if you want to create an Eclipse project per bundle, or create several bundles in a single Eclipse project. Multiple bundles in a single Eclipse project share the same build path (your compile path) which has no effect on the bundle output, but is easy to work with in Eclipse. The guideline is to put bundles that are functionally related in the same Eclipse project. We are going to start by creating an API bundle. An API bundle contains exported/public classes and interfaces, but should not contain implementations. Start by creating a new Bundle Descriptor in the <code>agenda</code> project, and name it <code>api</code>. If Bndtools asks to enable sub-bundles, accept the default (yes) and click Finish. If you take a look in the generated folder there should now be an empty JAR file <code>agenda.api.jar</code>. Double click on it to open the JAR File Viewer and inspect to see if it is really empty. The JAR File Viewer is a handy tool that might come of use later on.  
Next, add a new class <code>agenda.api.Conference</code>, and a new interface <code>agenda.api.Agenda</code>. The <code>Conference</code> class should have the following properties:

	String name;
	String location;

Also, implement a no-argument constructor and a convenience constructor that accepts the <code>name</code> and <code>location</code> as parameters. The <code>Agenda</code> interface should have two methods:

	List<Conference> listConferences();
	void addConference(Conference conference);

We should now add these API classes to the bundle. Open the <code>api.bnd</code> file and drag the <code>agenda.api</code> package to the Exported Packages area. Save the <code>api.bnd</code> file and take another look at the generated jar file using the JAR File Viewer. It should now contain the <code>Conference</code> class and the <code>Agenda</code> interface. The <code>META-INF/manifest.mf</code> file should now contain an export header:

    Export-Package: agenda.api;version="1.0"

You are now done creating your first bundle. The <code>agenda.api</code> can now be used by other bundles.

> By default, all packages you put in a bundle are only visible to code within that bundle. You need to explicitly export a package to share it with other bundles. When sharing a package, you can also version it. Bundles that use your package must import it. When importing a package, a bundle can specify a whole range of compatible versions. Whilst exports must be explicitly defined, imports can usually be determined automatically by Bndtools.

Step 2: Creating an OSGi service
--------------------------------
Next we will create the first OSGi service that implements the <code>Agenda</code> interface. It should be packaged in a separate bundle so that we can easily switch implementations. Create another bundle by creating a new Bundle Descriptor named <code>service.simple</code>.
Now add a new class <code>agenda.service.simple.SimpleAgendaService</code> that implements the <code>Agenda</code> interface. This implementation of the agenda should store conferences in-memory, so you can simply use a thread-safe list to store the conferences. For example:
   
    private List<Conference> conferences = new CopyOnWriteArrayList<>();

Implement both interface methods.

The first <code>Agenda</code> service implementation is now completed. As you can see the service implementation is just plain Java, nothing fancy or OSGi related required. We do have to register the service into the OSGi Service Registry however. OSGi has a low level API for this, but you shouldn't use this when doing application development. Instead you should use a dependency injection framework to do so. There are several choices such as Apache Felix Dependency Manager, Declarative Services and BluePrint. In this lab we will use Apache Felix Dependency Manager. This is the most powerful framework and has both a Java API and annotations based API. In this lab we will use annotations. 

We first have to include Felix Dependency Manager to our build path. We could do so manually in the build section of the bnd.bnd file, but we can also use Amdatu Bootstrap to do this for us.
> Download the latest version of Amdatu bootstrap from: <https://bitbucket.org/amdatu/amdatu-bootstrap/downloads/> and extract to get <code>bootstrap.jar</code> and the <code>conf</code> folder.

Start Amdatu bootstrap from the console: <code>java -jar bootstrap.jar</code>. It will open a browser window with the Amdatu Bootstrap UI. Navigate to your workspace and into the agenda project.
Type <code>workspace-addRepositories</code> to add the Amdatu repositories to the list of linked OSGi Bundle Repositories (OBR). Check the Repositories view in Eclipse to check the addition of the remote Amdatu repositories.
Now type <code>dependencymanager-install</code> in the command field, and select the <code>useAnnotations</code> option. Switch back to Bndtools and open the <code>bnd.bnd</code> file and go to the Build tab. The build path should now contain <code>org.apache.felix.dependencymanager</code> and the jar file containing the Apache Felix Dependency Manager annotations. Also take a look at the source tab of the bnd file to make yourself familiar with its format. You will be editing this file manually quite often.

Let's annotate our Agenda implementation with <code>@Component</code>. This will register the class as an OSGi service. Alternatively we could have done the same using an Activator class:

    manager.add(createComponent()
		.setInterface(Agenda.class.getName(), null)
		.setImplementation(SimpleAgendaService.class));

It's a matter of preference to use annotations or the Java API; technically the result is exactly the same.

We have to explicitly add the newly created class to the <code>service.simple</code> bundle. Open the <code>service.simple.bnd</code> file and drag the <code>agenda.service.simple</code> package to the Private Packages area. The classes are now added to the bundle JAR file, but will not be exported. Other bundles will not be able to see or use the classes, and that's exactly what we want. Implementation classes should be hidden from other bundles, only APIs should be shared. If we would have used an Activator instead of annotations we would also have to configure the Activator in the manifest. You can do this on the Contents tab of the <code>services.simple.bnd</code> file.

Step 3: Running the code
------------------------
Now that we have a service it would be interesting to actually run the code. Running an OSGi application is done by starting an OSGi framework with our application bundles and all dependent bundles installed. Bndtools makes this very easy. Create a new Run Descriptor in the project, using the <code>Apache Felix 4 (empty)</code> template, open it and go to the Run tab. First we need to specify the OSGi Framework that we will be using. Choose Apache Felix from the drop-down list, and set the Java execution environment as well. On the right hand side you will see Run Requirements and Run Bundles. The Run Bundles is a list of bundles that will be installed when you run the application. The Run Requirements is a way to generate the list of Run Bundles based on the requirements of bundles, but this mechanism doesn't work well in larger applications, so we discourage to use it. We manually control our Run Bundles instead.

Use the 'small brick with green plus' icon to add the following list of bundles to the Run Bundles:

* agenda.service.simple
* org.apache.felix.gogo.command
* org.apache.felix.gogo.runtime
* org.apache.felix.gogo.shell

Gogo is an OSGi console that you can use to see the list of installed bundles, stop and start bundles, view the log etc. Note that we did not add our API bundle and Apache Felix Dependency Manager yet, but before we do we should see what happens.

Now start the framework by clicking the green Run OSGi button. In the Eclipse console you will now see the OSGi shell. It shows an exception explaining that some packages can't be resolved. The root cause is shown if you scroll all the way to the right in the console. In this case it tells us there are packages from <code>agenda.api</code> missing. This is correct, because we didn't add this bundle to our framework yet.

You can view the list of installed bundles by typing <code>lb</code> in the console window. The <code>agenda.service.simple</code> bundle is in <code>INSTALLED</code> state, which means there are unresolved packages. Let's fix the resolver problem by adding our API bundle to the Run Bundles list as well. All bundles should now be resolved. Our service still doesn't run however because the Apache Felix Dependency Manager is still missing. Switch to Amdatu Bootstrap and execute the <code>dependencymanager-run</code> command with the name of the .bndrun file as argument to fix this. In the run configuration you should now see bundles added for Apache Felix Dependency Manager and its dependencies.


The bundle state should be <code>ACTIVE</code> for all bundles. This means the bundles are started, and all required imports are resolved. You can explicitly stop and start bundles with the <code>stop</code> and <code>start</code> commands.

When a bundle is <code>ACTIVE</code> it only says that all imports have been resolved and the <code>BundleActivator</code>'s start method has been invoked. It doesn't say anything about the availability of services it might register. Services are dynamic and can be registered and unregistered while the bundle is <code>ACTIVE</code>, even without restarting the bundle. You can get more information about registered services and dependencies between services by using the <code>dm</code> command. For example you could look up the <code>id</code> of the <code>agenda.service.simple</code> service using the <code>lb</code> command and then type <code>dm [id]</code>. It should show something similar to this:

    [9] agenda.service.simple
      agenda.api.Agenda() registered

Our <code>Agenda</code> service implementation is not doing much yet; it's registered, but nobody is using it. Let's start by adding some test data to the list of conferences when the service starts. For this you can add a <code>start()</code> method to the service implementation class. This is Felix Dependency Manager specific, but most alternative frameworks support something similar. Here is what an example <code>start()</code> method might look like:

    @Start
    public void start() {
    	conferences.add(new Conference("Devoxx", "Antwerp"));
    	conferences.add(new Conference("JFokus", "Stockholm"));

    	System.out.println("Added " + conferences.size() + " conferences");
    }

After saving your code changes, take a look at the console. What does it say?
    	
Step 4: Using the Agenda Service
--------------------------------
Let's create a consumer for the <code>Agenda</code> Service. We will create a RESTful web service on top of the <code>Agenda</code> service later on in the lab, but for now we will keep things within the OSGi shell. We will create a shell command that we can use from the OSGi shell to add and list conferences. Note that the OSGi shell is not meant to be used as an end-user shell. It's just useful for debugging purposes, but we can now use it to test interaction with our <code>Agenda</code> service.

Create a new Bundle Descriptor named <code>console</code>. Add a new class <code>agenda.console.AgendaConsole</code>. This class doesn't implement or extend any interfaces or base classes, it's just a POJO. First we will add an instance field for the <code>Agenda</code> service dependency.

    private volatile Agenda agenda;

Please note the usage of the Java <code>volatile</code> keyword. This is because the reference will be injected by the Felix Dependency Manager and because of service dynamics it might or might not be there at a given point in time. By making it <code>volatile</code> we ensure that visible in all threads that might be using it. We will further explore service dynamics later on. Next, add two methods that will be invoked by the shell:

    public void listConferences() {
  		List<Conference> conferences = agenda.listConferences();
  		for (Conference conference : conferences) {
  			System.out.println(conference.getName());
  		}
  	}

  	public void addConference(String name, String location) {
  		agenda.addConference(new Conference(name, location));
  	}

The reference to the <code>Agenda</code> service should be looked-up from the Service Registry. Just as with service registration you should use a dependency injection framework for this instead of using the low level APIs. The low level APIs require you to deal with thread safety; remember that services can come and go at any time. This is non-trivial and a lot of work. With a dependency injection framework it's very easy however, as the framework will take care of the dynamics. To do so we have to register the component and declare a dependency on the <code>Agenda</code> service. We can use annotations again, but just to see both approaches we will use an activator for this component. With Felix Dependency Manager create an <code>Activator</code> class that extends DependencyActivatorBase.  
Our shell command doesn't implement any special Java interface. It's just a POJOby, so the 'interface' of our service will be <code>Object.class</code>. This is to say that our component has the lifecycle of a service, but is not meant to be used from other Java classes. How does our command get registered in the shell however? For that we use so-called service properties, to flag the service as a command. 

    @Override
  	public void init(BundleContext context, DependencyManager manager)
  			throws Exception {
  		Properties props = new Properties();
  		props.put(CommandProcessor.COMMAND_SCOPE, "agenda");
  		props.put(CommandProcessor.COMMAND_FUNCTION, new String[] {
  				"listConferences", "addConference" });

  		manager.add(createComponent()
  			.setInterface(Object.class.getName(), props)
  			.setImplementation(AgendaConsole.class)
  			.add(createServiceDependency().setService(Agenda.class)));
  	}
  	
To be able to use the <code>CommandProcessor</code>, we need to add <code>org.apache.felix.gogo.runtime</code> to our build path.
Gogo will pick up any service that has those service properties and register the service as a shell command. This is what is called "whiteboard style" registration, and you will see several implementations of this pattern in different frameworks. Finally, add the <code>agenda.console</code> package to the <code>console.bnd</code> Private Packages and don't forget to configure the <code>Activator</code>.  
As soon as you save the <code>console.bnd</code> file, add it to your <code>.bndrun</code> Run Bundles list, and you should be able to see the new bundle with <code>lb</code> if the framework is still running. This is one of the great things of working with OSGi and Bndtools; hot code deployments, and almost never restarting a server.

Type <code>help</code> in the console. You should see the two new commands in the list of commands. Try adding a conference:

    addConference JavaOne SanFrancisco

And try listing conferences to see if it worked:

    listConferences


Step 5: Understanding service dynamics
--------------------------------------
OSGi services are dynamic; they can be registered and de-registered at runtime. What does that mean exactly, and when would that happen?  
In an OSGi environment it is perfectly possible to update components or change configuration of a running application. We don't have to take the application down to do the update, this can all be done without ever restarting. We could also start new services when new configuration is added for example. This gives a lot of flexibility when deploying an application, but your code needs to be able to handle this situation. 

Let's say for example that the <code>SimpleAgenda</code> service is being updated. To update the bundle, the bundle has to be stopped, updated, and started again. What happens if some other service (like our command) tries to invoke the <code>Agenda</code> service while the bundle is stopped? We can try this by stopping the <code>agenda.service.simple</code> bundle and then issue the <code>listConferences</code> command. 

That will result in a <code>NullPointerException</code>. The reason for that is that by default Felix Dependency Manager will inject an object that returns <code>null</code> for every method invocation when the service is unavailable. That means that the <code>conferences</code> variable in the following example will be <code>null</code>:

    List<Conference> conferences = agenda.listConferences();
    
The iteration over this variable will throw the <code>NullPointerException</code>. We could try to handle <code>NullPointerException</code>s ourselves in case our service dependencies are not available. In our command for example we could return a user-friendly message when the <code>Agenda</code> is not available:

    public void listConferences() {
  		List<Conference> conferences = agenda.listConferences();
  		if(conferences != null) {
  			for (Conference conference : conferences) {
  				System.out.println(conference.getName());
  			}
  		} else {
  			System.out.println("Agenda not available");
  		}
  	} 
  	
Save your code change and run <code>listConferences</code> again in the console. What does it say now?  
While this may work in many cases, it is still unsafe, as checking and using the <code>conferences</code> variable are not in an atomic operation and the value might change in between. It would be much better if the service that requires the unavailable service were also unregistered in a consistent way, when its dependencies are missing. Felix Dependency Manager can do this automatically. All you have to do is set the service dependency as <code>required</code> in the <code>Activator</code>:

    manager.add(createComponent()
    	.setInterface(Object.class.getName(), props)
    	.setImplementation(AgendaConsole.class)
    	.add(createServiceDependency()
			.setService(Agenda.class)
			.setRequired(true)));

Note that when using Apache Felix Dependency Manager annotations, the default for a <code>@ServiceDependency</code> is required. 

Let's test stopping the <code>agenda.service.simple</code> bundle again, and use the <code>listConferences</code> command. The shell will now show an error that the command doesn't exist. Because the command service has a required dependency on the <code>Agenda</code> service, the whole service will be deregistered when the <code>Agenda</code> is unavailable. Another way to check for missing dynamic dependencies is using the command <code>dm notavail</code>.  
When the <code>Agenda</code> service becomes available again (<code>start</code> the <code>agenda.service.simple</code> bundle in the console) the command will automatically register again as well.

*As you can see dynamic services are a very powerful mechanism. It gives you several options of dealing with (temporarily) unavailable components which can be very helpful for deployments. Dealing with dynamics can be as easy as letting the framework handle unavailable services, or as complex as a complete fallback mechanism to gracefully degrade the system in specific cases. But what happens if there are two services registered for the same interface? You will see that in part 2 of this hands-on lab!*
