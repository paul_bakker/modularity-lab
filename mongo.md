---
title: Modularity Lab - Using MongoDB
author: Luminis
layout: base
---

Part 2 - Using NoSQL with Mongo
==============================

This exercise builds upon the previously created project where we have explored the basics of a modular approach towards application development. Here we will be enhancing our application by adding another service implementation for the Agenda service. This implementation will use persistent back-end storage. In this lab we will store the conferences in a MongoDB database. MongoDB is a highly popular NoSQL database that offers document-oriented storage, full index support, replication & high availability, querying, and map/reduce support amongst many other features. This will be a perfect fit for building our modular enterprise application in the cloud.

Since we are no longer just doing service basics, we might want to use some higher level APIs for using frameworks or in the case of this lab, dealing with persistent storage. For this we will introduce Amdatu. Amdatu is an open source community effort focussed on making OSGi development more accessible. It contains components to create RESTful, scalable and distributed web applications that use NoSQL data stores  and much more. This is exactly the answer to the need that we have right now, so it is a perfect fit and we will include some of Amdatu's components in the next couple of labs.


Getting started
---------------
Before we can do anything with MongoDB, we have to make sure it is running on our system. Below are some installation instructions for Mac OS X, Linux 64-bit, and Windows 32/64-bit systems. You can grab the MongoDB binaries from the lab installation files you received at the beginning of this lab.

###Installing MongoDB on Mac OS X or Linux#
Open a Terminal session and issue the following command to extract the files from the archive:

	tar -zxvf mongo.tgz
	
Optionally, move the extracted folder into a more generic location if you like.

You can find the <code>mongod</code> binary, and the binaries of all the associated MongoDB utilities, in the <code>bin/</code> directory within the archive. Before you start <code>mongod</code> for the first time, you will need to create the <code>data</code> directory. By default, <code>mongod</code> writes data to the <code>/data/db/</code> directory. To create this directory, and set the appropriate permissions use the following commands:

	sudo mkdir -p /data/db
	sudo chown `id -u` /data/db

For testing purposes, you can start a <code>mongod</code> directly in the terminal:

	mongod

Among the tools included with this MongoDB distribution, is the <code>mongo</code> shell. You can use this shell to connect to your MongoDB instance by issuing the following command at the system prompt from inside of the directory where you extracted mongo:

	./bin/mongo
	
This will connect to the database running on the <code>localhost</code> interface by default. At the mongo prompt, issue the following two commands to insert a record in the “test” collection of the (default) “test” database and then retrieve that record:

	> db.test.save( { a: 1 } )
	> db.test.find()
	
###Installing MongoDB on Windows#
Please make sure you choose the correct version of MongoDB for your Windows system. The 64-bit versions of MongoDB will not work with 32-bit Windows.

*Please note: as of version 2.2 MongoDB does not support Windows XP. Please use a more recent version of Windows to use more recent releases of MongoDB.*

In Windows Explorer, find the MongoDB zip file in the lab installation folder. Extract the archive to a directory of your liking. MongoDB is self-contained and does not have any other system dependencies. You can run MongoDB from any folder you choose.

MongoDB requires a <code>data</code> folder to store its files. The default location for the MongoDB data directory is <code>C:\data\db</code>. Create this folder using the Command Prompt. Issue the following command sequence:

	md data
	md data\db
	
To start MongoDB, navigate to Mongo's <code>/bin</code> sub-directory and execute from the Command Prompt:

	mongod.exe
	
Connect to MongoDB using the <code>mongo.exe</code> shell. Open another Command Prompt and issue the following command:

	mongo.exe
	
The <code>mongo.exe</code> shell will connect to <code>mongod.exe</code> running on the <code>localhost</code> interface and port 27017 by default. At the <code>mongo.exe</code> prompt, issue the following two commands to insert a record in the "test" collection of the default "test" database and then retrieve that record:

	> db.test.save( { a: 1 } )
	> db.test.find()


Step 1: Create a new project and define dependencies
----------------------------------------------------
After setting up MongoDB, we are now ready to create a new service. Create a new Bundle Descriptor to contain a new service implementation called <code>service.mongo</code>. For this we need some MongoDB related services and a MongoDB Java driver. Furthermore we need some Jackson bundles to perform the JSON to Mongo (BSON) serialization. Use the <code>mongo-install</code> command in Amdatu Bootstrap to install the required dependencies and choose MongoJack as mapping framework.

Now, you are all set to write some more code.

Step 2: Implement the persistent service
----------------------------------------
Create a new class called <code>MongoAgendaService</code> in a package named <code>agenda.service.mongo</code>. Implement the <code>Agenda</code> interface and make sure you add the unimplemented methods. In order to interact with Mongo we can make use of the Amdatu Mongo service component. Amdatu exposes a service called <code>MongoDBService</code> that can be used to interact with Mongo without the hassle of setting up the connection programmatically. For this we need to add the <code>@Component</code> annotation on the new class, and declare a dependency to this service:

	@ServiceDependency
	private volatile MongoDBService mongoDbService;

Let's setup the mapping framework in the <code>start()</code> method. 

	private volatile JacksonDBCollection<Conference, String> conferences;
	
	@Start
	public void start() {
		DBCollection collection = mongoDbService.getDB().getCollection("conferences");
		conferences = JacksonDBCollection.wrap(collection, Conference.class, String.class);
	}

The <code>listConferences()</code> uses the wrapped conferences collection to execute a query and return the result. In Mongo terms a collection is roughly comparable to a table in a relational database. 

	@Override
	public List<Conference> listConferences() {
		
		List<Conference> result = new ArrayList<>();
		
		conferences.find().forEach(result::add);
		
		return result;
	}

The <code>addConference()</code> is even simpler. Using the wrapped collection again we can simple <code>save()</code> a Conference without any extra work.

	public void addConference(Conference conference) {
		conferences.save(conference);
	}
	
Our component is now ready to be used, we just need to make sure it's packaged in a bundle. Open the <code>service.mongo.bnd</code> configuration and drag the <code>agenda.service.mongo</code> package to the Private Packages. Now add the bundle to the list of bundles in the .bndrun file. We also need to add the Mongo and MongoJack dependencies; use the Amdatu Bootstrap <code>mongo-run</code> command for this.


Step 3: Dealing with the right service
--------------------------------------
If everything works as expected you might be surprised, or disappointed, or both, to see that there was indeed some output on the console, but it still is the output that comes from our simple in-memory implementation we created in an earlier exercise. How come? Somehow we see that some "random" implementation of the service implementation was chosen. So, how do we differentiate between multiple implementations of the same service? There are a couple of options to deal with this. First we can rely on the runtime to pick one. If all we need is just some implementation of the <code>Agenda</code> API, we are probably fine with whatever we can get.
The OSGi runtime will give us one based on a couple of rules:

* If the services have a property called the *service.ranking* its numerical value is used to determine the highest ranked service and return it. Services with no ranking get the ranking of *0*.
* If there is a tie, or if no services have a ranking, the service that was registered first is chosen. You can always see which one that is, because each service is registered with a property called *service.id* and this ID will always count up, but remain the same as long as the service is registered. Never, ever, depend on the start order of services though! This will be different on each deployment and you should not rely on this!

Alternatively we can just stop one of the two service implementations by hand, by using <code>stop</code> and <code>start</code> commands from the console. However, if we want to exercise more control over what implementation the runtime should pick, then two other options are available that help us to choose the desired implementation:

*	Using service ranking
*	Using service properties

For demonstration purposes we will use the latter to indicate the desired implementation. For this we need two changes to the code. First we need to add a service property to our newly created persistent service. Using the service property the service can indicate to the runtime it has some special ability. To add the service property you can add a list of properties to the DependencyManager when registering the service implementation. Simply declare service properties in the <code>@Component</code> annotation, with for example <code>persistent=true</code> as its name/value. 

    @Component(properties = @Property(name="persistent", value="true"))

Now in order to instruct the runtime to not just look for any implementation of the <code>Agenda</code>, but to look for the implementation that has the persistent property instead, we can use a filter expression when declaring the service dependency in the <code>Activator</code> of our client, the <code>AgendaConsole</code>. The filter expressions that can be used here are similar to an LDAP syntax. After applying the filter, the <code>init()</code> method of the client's <code>Activator</code> will look like this:

	manager.add(createComponent()
		.setInterface(Object.class.getName(), props)
		.setImplementation(AgendaConsole.class)
		.add(createServiceDependency()
			.setService(Agenda.class, "(persistent=true)")
			.setRequired(true)));

If you kept up with the exercise this far, you will probably notice that everything is set up as planned but when trying to invoke the <code>listConferences</code> command again you will run into some new problems. Let's try and find the reason for this. First off, list the bundles deployed in the runtime by running the <code>lb</code> command in the shell. You will find that all bundles have successfully loaded and are all marked as <code>ACTIVE</code>. 

Now, find the <code>id</code> of our client console and run the <code>dm</code> command on it to see if its dependencies are resolved in a satisfactory way. You will find that the client console doesn't have a required service available. The required service is of type <code>Agenda</code> and has the <code>persistent=true</code> filter. This seems to be our newly created <code>MongoAgendaService</code>. How come? Lets run the <code>dm</code> command again, but this time on the <code>id</code> of our <code>agenda.service.mongo</code> implementation. What does it say?

So apparently the <code>MongoDBService</code> is unavailable. We did however set up MongoDB on our system and deploy the necessary dependencies in our runtime. Still, the <code>MongoDBService</code> was unable to start. This is because the <code>MongoDBService</code> needs some mandatory configuration in order to know to what database to connect to. The Amdatu MongoDBService uses the Managed Service Factory pattern and in order to bootstrap it we need to supply a configuration file. The configuration file needs to adhere to the metatype configuration file specification. 

In order to supply the configuration file we need to create a new folder in our <code>agenda</code> project. Create a new folder called <code>conf</code>. We will use the *Amdatu Configurator* component to load configuration from an xml file in this folder. Next, drag the <code>org.amdatu.mongo-demo.xml</code> file from the <code>conf</code> sub-folder from the lab files into the newly created <code>conf</code> folder. Open the configuration file and admire it. There is not much to it, but it holds the name of our database and the address where it will connect. In order for the configuration file to be picked up by the runtime, you need to supply a couple of extra bundles to your Run Bundles in the .bndrun file:

* org.amdatu.configurator.api
* org.amdatu.configurator.autoconf

In some cases you might have to restart the OSGi container in order for it to pick it up.

Now, lets <code>lb</code> again to list all of our bundles. Then run <code>dm notavail</code> to check whether there still are some unresolved dependencies. If all is well we should be all right now. Use the <code>listConferences</code> command to list all of the conferences. Oops. Empty list. This proves that we are now using the MongoDB database, but unfortunately there is nothing in it yet. One way to get some data in the database is to use the <code>mongo</code> shell to add a conference like this:

	> use osgi-lab
	switched to db osgi-lab
	
	> db.conferences.save({"name": "OSGi in One Day", "location": "Bucharest"})
	> db.conferences.find()
	
By now, our database contains an entry for Devoxx. Let's run the <code>listConferences</code> command again to check if our service is able to retrieve it as well.

Dang! We ran into some trouble again. This time there seems to be something wrong with the serialization of the <code>Conference</code> type. There are a couple of ways to solve this, but by far the easiest way is to add a member variable called <code>_id</code> to the <code>Conference</code> class as Mongo requires this. 

    @ObjectId
	private String _id;

Give it a getter and setter as well. Now, run the <code>listConferences</code> command again and you should be all set!

Finally, now that our persistent service and Mongo play nice with each other, let's test our <code>addConference</code> command as well. Use <code>listConferences</code> again to see if the desired result was achieved.

*Congratulations, you have worked your way down to the final bit of part 2 of this lab. Please continue with Part 3 to learn all about REST-enabling your services, making them even more cloud-ready.*